package com.example.kefir.lab11;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.app.AlertDialog;
import android.app.Dialog;
import android.view.View;
import android.widget.Toast;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnCancelListener;
import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import 	android.content.Intent;


import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    TextView textForPuink;
    Button puink;
    Button review;
    AlertDialog.Builder ad;
    Context context;
    final int IDD_THREE_BUTTONS = 2;
    int itemGlob;
    ProgressBar proBar;
    Button goTo2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // найдем View-элементы
        textForPuink = (TextView) findViewById(R.id.textForPuink);
        puink = (Button) findViewById(R.id.puink);
        review = (Button) findViewById(R.id.review);
        proBar = (ProgressBar) findViewById(R.id.proBar);
        goTo2 = (Button) findViewById(R.id.goButt1);

        proBar.setVisibility(ProgressBar.INVISIBLE);

        View.OnClickListener oclPuink = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                textForPuink.setText(R.string.myname);

            }};
        puink.setOnClickListener(oclPuink);

        View.OnClickListener oclReview = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ad.show();
                showDialog(IDD_THREE_BUTTONS);
            }};
        review.setOnClickListener(oclReview);

        View.OnClickListener oclGoTo2 = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent intent = new Intent(v.getContext(), Main2Activity.class);
                startActivity(intent);

            }};
        goTo2.setOnClickListener(oclGoTo2);
    };

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case IDD_THREE_BUTTONS:
                final String[] points = {getString(R.string.one), getString(R.string.two), getString(R.string.three),
                        getString(R.string.four), getString(R.string.five)};
                AlertDialog.Builder builder = new AlertDialog.Builder(this);

                final EditText input = new EditText(MainActivity.this);
                input.setHint(getString(R.string.yourRew));
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT);
                input.setLayoutParams(lp);
                builder.setView(input);

                builder.setTitle(getString(R.string.getRew))

                        // добавляем переключатели
                        .setSingleChoiceItems(points, -1,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog,
                                                        int item) {
                                        itemGlob = item;
                                    }
                                })
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.review),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {

                                        proBar.setVisibility(ProgressBar.VISIBLE);
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            public void run() {
                                                // Actions to do after 2 seconds
                                                proBar.setVisibility(ProgressBar.INVISIBLE);
                                                Toast.makeText(
                                                        getApplicationContext(),
                                                        getString(R.string.tnx),
                                                        Toast.LENGTH_SHORT).show();

                                            }
                                        }, 2000);
                                        dialog.cancel();
                                    }
                                })
                        // добавляем одну кнопку для закрытия диалога
                        .setNeutralButton(getString(R.string.close),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int id) {
                                        dialog.cancel();
                                    }
                                })

                ;
                return builder.create();
            default:
                return null;
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.action1) {
            // Handle the camera action
            puink = (Button) findViewById(R.id.puink);
            textForPuink.setText(R.string.myname);

        } else if (id == R.id.action2) {
            puink = (Button) findViewById(R.id.puink);
            textForPuink.setText(R.string.notPuink);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
